var http = require('http');

var server =  http.createServer( (req, res) => {
   res.writeHead(200,{"content-type": "text/plain"});
   res.end('Welcome to tech brothers');
});

server.listen(3000,"127.0.0.1");
console.log('3000 port started listening');
